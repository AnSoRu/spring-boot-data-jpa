package com.bolsadeideas.springboot.app.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LocaleController {

	@GetMapping("/locale")
	public String locale(HttpServletRequest httpServletRequest) {//Automaticamente Spring lo inyecta
		String ultimaUrl = httpServletRequest.getHeader("referer"); //Es el link de la última página
		return "redirect:" + ultimaUrl;
	}
}
