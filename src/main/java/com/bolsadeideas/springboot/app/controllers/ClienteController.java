package com.bolsadeideas.springboot.app.controllers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.Cliente;
import com.bolsadeideas.springboot.app.models.services.IClienteService;
import com.bolsadeideas.springboot.app.models.services.IUploadFileService;
import com.bolsadeideas.springboot.app.util.paginator.PageRender;
import com.bolsadeideas.springboot.app.view.xml.ClienteList;

@Controller
@SessionAttributes("cliente")
public class ClienteController {

	/*@Autowired
	@Qualifier("clienteDaoJPA")
	private IClienteDao clienteDao;*/
	
	protected final Log logger = LogFactory.getLog(this.getClass());

	@Autowired
	private IClienteService clienteService;

	@Autowired
	private IUploadFileService uploadFileService;
	
	@Autowired
	private MessageSource messageSource;


	//Para cargar programaticamente un recurso de otra forma diferente a la clase MvcConfig.java
	@Secured({"ROLE_USER","ROLE_ADMIN"})
	@GetMapping(value="/uploads/{filename:.+}")
	public ResponseEntity<Resource> verFoto(@PathVariable String filename){

		Resource recurso = null;
		try {
			recurso = uploadFileService.load(filename);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/*Path pathFoto = Paths.get(UPLOADS_FOLDER).resolve(filename).toAbsolutePath();
		logger.info("pathFoto: " + pathFoto);
		Resource recurso = null;
		try {
			recurso = new UrlResource(pathFoto.toUri());
			if(!recurso.exists() || !recurso.isReadable()) {
				throw new RuntimeException("Error: no se puede cargar la imagen: " + pathFoto.toString());
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}*/
		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,"attachment; filename=\"" +
				recurso.getFilename() + "\"").body(recurso);
	}

	@PreAuthorize("hasAnyRole('ROLE_USER')")
	@GetMapping(value="/ver/{id}")
	public String ver(@PathVariable(value = "id") Long id, Map<String,Object> model, RedirectAttributes flash) {

		//Cliente cliente = clienteService.findOne(id);
		Cliente cliente = clienteService.fetchByIdWithFacturas(id);
		if(cliente == null) {
			flash.addFlashAttribute("error","El cliente no existe en la base de datos");
			return "redirect:/listar";
		}
		model.put("cliente",cliente);
		model.put("titulo","Detalle cliente " + cliente.getNombre());

		return "ver";
	}
	
	//Este metodo devuelve los datos tanto en JSON como en XML si se devuelve nuestra clase ClienteList
	//Por defecto toma XML hay que añadir el parámetro ?format=json ó ?format=xml
	@GetMapping(value = "/listar-rest")//Por defecto es GET
	@ResponseBody //Significa que el listado se va a almacenar en el cuerpo de la respuesta. Spring ya deduce que tiene que ser tipo JSON o XML que no es una Vista
	public ClienteList listarRest() {
		return new ClienteList(clienteService.findAll());
	}

	@RequestMapping(value = {"/listar", "/"}, method = RequestMethod.GET)//Por defecto es GET
	public String listar(@RequestParam(name="page",defaultValue="0") int page, Model model,
			Authentication authentication, HttpServletRequest request, Locale locale) {
		
		//Ejemplo de comprobar la autenticacion de forma programatica de forma adicional a utilizar el th:sec
		
		if(authentication != null) {
			logger.info("Hola usuario autenticado, tu username es: ".concat(authentication.getName()));
		}
		//Otra forma de obtener el authentication en lugar de pasarlo como parametro
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		if(auth != null) {
			logger.info("Utilizando forma estática SecurityContextHolder.getContext().getAuthentication(): Usuario autenticado, username: ".concat(auth.getName()));
		}
		
		if(hasRole("ROLE_ADMIN")) {
			logger.info("Hola ".concat(auth.getName()).concat(" tienes acceso!"));
		}else {
			logger.info("Hola ".concat(auth.getName()).concat(" NO tienes acceso!"));
		}
		
		//Es otra alternativa de validar el ROLE
		SecurityContextHolderAwareRequestWrapper securityContext = new SecurityContextHolderAwareRequestWrapper(request,"ROLE_");
		if(securityContext.isUserInRole("ADMIN")) {
			logger.info("Forma usando SecurityContextHolderAwareRequestWrapper: Hola ".concat(auth.getName()).concat(" tienes acceso!"));
		}else {
			logger.info("Forma usando SecurityContextHolderAwareRequestWrapper: Hola ".concat(auth.getName()).concat(" NO tienes acceso!"));
		}
		
		//Otra alternativa de validar el ROLE
		if(request.isUserInRole("ROLE_ADMIN")) {
			logger.info("Forma usando HttpServletRequest: Hola ".concat(auth.getName()).concat(" tienes acceso!"));
		}else {
			logger.info("Forma usando HttpServletRequest: Hola ".concat(auth.getName()).concat(" NO tienes acceso!"));
		}

		Pageable pageRequest = PageRequest.of(page,4); //Por ejemplo paginamos por cada 4 registros
		Page<Cliente> clientes = clienteService.findAll(pageRequest); //Esta es la lista paginada

		PageRender<Cliente> pageRender = new PageRender<>("/listar",clientes);

		//model.addAttribute("titulo","Listado de clientes");
		model.addAttribute("titulo",messageSource.getMessage("text.cliente.listar.titulo",null,locale)); //Para implementar el lenguaje
		//model.addAttribute("clientes",clienteService.findAll());
		model.addAttribute("clientes",clientes);
		model.addAttribute("page",pageRender);
		return "listar";
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/form")
	public String crear(Map<String,Object> model) {
		Cliente cliente = new Cliente();
		model.put("cliente",cliente);
		model.put("titulo","Formulario de Cliente");
		return "form";
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/form/{id}")
	public String editar(@PathVariable(value="id") Long id, Map<String,Object> model,RedirectAttributes flash) {

		Cliente cliente = null;
		if(id > 0) {
			cliente = clienteService.findOne(id);
			if(cliente == null) {
				flash.addFlashAttribute("error","El ID del cliente no existe en la BBDD!");
			}
		}else {
			flash.addFlashAttribute("error","El ID del cliente no puede ser cero!");
			return "redirect:/listar";
		}
		model.put("cliente",cliente);
		model.put("titulo","Editar cliente");
		model.put("tituloBoton", "Editar cliente");
		return "form";
	}

	//@Secured("ROLE_ADMIN")
	@PreAuthorize("hasRole('ROLE_ADMIN')") //Es otra forma, previamente hay que añadir el PreAuthorize en la clase SpringSecurityConfig
	@RequestMapping(value="/form",method = RequestMethod.POST)
	public String guardar(@Valid @ModelAttribute("cliente") Cliente cliente, BindingResult bindingResult, Model model, @RequestParam("file") MultipartFile foto,RedirectAttributes flash, SessionStatus sessionStatus) {//Acordarse que el BindingResult esta asociado al parametro que le precede
		if(bindingResult.hasErrors()){
			model.addAttribute("titulo","Formulario de Cliente");
			return "form";
		}
		if(!foto.isEmpty()) {
			//Path directorioRecursos = Paths.get("src//main//resources//static//uploads");
			//String rootPath = directorioRecursos.toFile().getAbsolutePath();
			//String rootPath = "C://Temp//uploads";

			if(cliente.getId() != null &&
					cliente.getId() > 0 &&
					cliente.getFoto() != null &&
					cliente.getFoto().length() > 0){
				//Aqui podemos eliminar la foto antigua
				/*Path rootPath = Paths.get(UPLOADS_FOLDER).resolve(cliente.getFoto()).toAbsolutePath();
				File archivo = rootPath.toFile();
				if(archivo.exists() && archivo.canRead()) {
					archivo.delete();
				}*/
				uploadFileService.delete(cliente.getFoto());
			}

			String uniqueFilename = null;
			try {
				uniqueFilename = uploadFileService.copy(foto);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			flash.addFlashAttribute("info","Ha subido correctamente '" + uniqueFilename + "'");

			cliente.setFoto(uniqueFilename);


		}

		String mensajeFlash = (cliente.getId() != null)? "Cliente editado con éxito!" : "Cliente creado con éxito!";
		clienteService.save(cliente);
		sessionStatus.setComplete();
		flash.addFlashAttribute("success",mensajeFlash);
		return "redirect:/listar";
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/eliminar/{id}")
	public String eliminar(@PathVariable(value="id") Long id, RedirectAttributes flash) {
		if(id > 0) {
			Cliente cliente = clienteService.findOne(id);

			flash.addFlashAttribute("success","Cliente eliminado con éxito!");
			clienteService.delete(id);

			/*Path rootPath = Paths.get(UPLOADS_FOLDER).resolve(cliente.getFoto()).toAbsolutePath();
		File archivo = rootPath.toFile();
		if(archivo.exists() && archivo.canRead()) {*/
			if(uploadFileService.delete(cliente.getFoto())){
				flash.addFlashAttribute("info","Foto " + cliente.getFoto() + " eliminada con éxito!");
			}
		}
		return "redirect:/listar";
	}
	
	
	private boolean hasRole(String role) {
		SecurityContext context = SecurityContextHolder.getContext();
		if(context == null) {
			return false;
		}
		Authentication auth = context.getAuthentication();
		if(auth == null) {
			return false;
		}
		Collection<? extends GrantedAuthority> authorities = auth.getAuthorities();//Todo role en Spring tiene que implementar el GrantedAuthority
		
		//1ªForma de comprobar
		/*for(GrantedAuthority authority: authorities) {
			if(role.equals(authority.getAuthority())) {
				logger.info("Hola usuario ".concat(auth.getName()).concat(" tu role es ").concat(authority.getAuthority()));
				return true;
			}
		}
		return false;*/
		
		//2ªForma de comprobar
		return authorities.contains(new SimpleGrantedAuthority(role));
	}
}
