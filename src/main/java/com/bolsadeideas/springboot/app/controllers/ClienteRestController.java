package com.bolsadeideas.springboot.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bolsadeideas.springboot.app.models.services.IClienteService;
import com.bolsadeideas.springboot.app.view.xml.ClienteList;

@RestController //Solo responde a JSON o XML
@RequestMapping("/api/clientes")
public class ClienteRestController {
	
	@Autowired
	private IClienteService clienteService;
	
	@GetMapping(value = "/listar")//Por defecto es GET
	@ResponseBody //Significa que el listado se va a almacenar en el cuerpo de la respuesta. Spring ya deduce que tiene que ser tipo JSON o XML que no es una Vista
	public ClienteList listar() {
		return new ClienteList(clienteService.findAll());
	}

}
