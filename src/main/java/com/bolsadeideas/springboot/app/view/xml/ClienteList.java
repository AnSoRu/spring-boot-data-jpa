package com.bolsadeideas.springboot.app.view.xml;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bolsadeideas.springboot.app.models.entity.Cliente;

@XmlRootElement(name="clientesList")
public class ClienteList {

	//Esta clase sirve para convertir a XML
	@XmlElement(name="cliente")
	public List<Cliente> clientes;

	//Muy importante crear el constructor vacío para que lo maneje el Marshalling
	public ClienteList() {

	}

	public ClienteList(List<Cliente> clientes) {
		this.clientes = clientes;
	}

	public List<Cliente> getClientes() {
		return clientes;
	}
}