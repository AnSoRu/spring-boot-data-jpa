package com.bolsadeideas.springboot.app.models.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.bolsadeideas.springboot.app.models.entity.Cliente;

public interface IClienteDao extends PagingAndSortingRepository<Cliente,Long>{
	
	//No es necesario añadir ninguna anotacion de Component porque CrudRepository ya es un Component
	
	/*Sin CRUDRepository
	public List<Cliente> findAll();
	public void save(Cliente cliente);
	public Cliente findOne(Long id);
	public void delete(Long id);
	*/
	
	//En lugar de extendr de CrudRepository<T,E> extendemos de PagingAndSortingRepository
	//el cual por debajo extiende de CrudRepository<T,E>
	
	//Se hace un left join para que traiga tambien a los Clientes que NO tengan facturas
	@Query("select c from Cliente c left join fetch c.facturas f where c.id=?1")
	public Cliente fetchByIdWithFacturas(Long id);

}
