package com.bolsadeideas.springboot.app.models.services;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class UploadFileServiceImpl implements IUploadFileService {

	private final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(getClass());

	private final static String UPLOADS_FOLDER = "uploads";


	@Override
	public Resource load(String fileName) throws MalformedURLException {
		Path pathFoto = getPath(fileName);
		logger.info("pathFoto: " + pathFoto);
		Resource recurso = null;
		recurso = new UrlResource(pathFoto.toUri());
		if(!recurso.exists() || !recurso.isReadable()) {
			throw new RuntimeException("Error: no se puede cargar la imagen: " + pathFoto.toString());
		}
		return recurso;
	}

	@Override
	public String copy(MultipartFile file) throws IOException {
		String uniqueFilename = UUID.randomUUID().toString().concat("_").concat(file.getOriginalFilename());

		Path rootPath = getPath(uniqueFilename);

		logger.info("rootPath: " + rootPath);


		//byte [] bytes = foto.getBytes();
		//Path rutaCompleta = Paths.get(rootPath + "//" + foto.getOriginalFilename());
		//Files.write(rutaCompleta, bytes);
		Files.copy(file.getInputStream(),rootPath);


		return uniqueFilename;
	}

	@Override
	public boolean delete(String fileName) {
		Path rootPath = getPath(fileName);
		File archivo = rootPath.toFile();
		if(archivo.exists() && archivo.canRead()) {
			if(archivo.delete()) {
				return true;
			}
		}
		return false;
	}

	public Path getPath(String filename) {
		return Paths.get(UPLOADS_FOLDER).resolve(filename).toAbsolutePath();
	}

	@Override
	public void deleteAll() {
		//Metodo para eliminar los archivos de la carpeta 'uploads'
		FileSystemUtils.deleteRecursively(Paths.get(UPLOADS_FOLDER).toFile());
	}

	@Override
	public void init() throws IOException {
		Files.createDirectory(Paths.get(UPLOADS_FOLDER));
	}

}
