package com.bolsadeideas.springboot.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.bolsadeideas.springboot.app.models.services.IUploadFileService;

@SpringBootApplication
public class SpringBootDataJpaApplication implements CommandLineRunner{
	
	@Autowired
	private IUploadFileService uploadFileService;
	
	//@Autowired
	//private BCryptPasswordEncoder passwordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDataJpaApplication.class, args);
	}

	//En este metodo especificamos las acciones a realizar cada vez
	/*
	 * que se inicializa desde 0 el proyecto.
	 * En este ejemplo creamos la carpeta 'uploads'
	 */
	@Override
	public void run(String... args) throws Exception {
		uploadFileService.deleteAll();
		uploadFileService.init();
		
		/*String password = "12345";
		
		for(int i = 0; i < 2; i++) {
			String bcryptPassword = passwordEncoder.encode(password); //Genera una encriptacion diferente cada vez para el mismo string
			System.out.println(bcryptPassword);
		}*/
	}
}